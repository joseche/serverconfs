#!/bin/bash


yum -y update
yum -y install epel-release
yum -y group install "Development Tools"
yum -y install python-devel
easy_install pip
pip install paramiko PyYAML Jinja2 six httplib2


